package br.com.lead.collector.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;

@Entity
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message = "Nome nao pode estar como vazio")
    @NotBlank(message =  "Nome não pode ser vazio")
    @Size(min = 5, message = "O nome deve ter mais que 5 caracteres")
    private String nome;

    @NotNull(message = "Nome nao pode estar como vazio")
    @NotBlank(message =  "Descrição não pode ser vazia")
    @Size(min = 5, message = "A descrição deve ter mais que 5 caracteres")
    private String descricao;

    @NotNull
    @Digits(integer = 6, fraction = 2, message = "Preço fora do padrão")        //seis casas decimais e duas frações 123456,12
    @DecimalMin("1.0")          //Preco minimo
    private float preco;


    public Produto() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Float getPreco() {
        return preco;
    }

    public void setPreco(Float preco) {
        this.preco = preco;
    }
}
