package br.com.lead.collector.controller;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoServices;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoServices produtoServices;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    //@RequestBody -- objeto vem do Json
    public Produto cadastrarProduto(@RequestBody @Valid Produto produto){
        //return produtoServices.salvarProduto(produto); --> outra forma de movimentar para sair
        Produto objetoProduto = produtoServices.salvarProduto(produto);
        return objetoProduto;
    }

    @GetMapping
    public Iterable<Produto> lerTodosOsProdutos(){
        return produtoServices.lerTodosOsProdutos();
    }

    @GetMapping("/{id}")
    public Produto pesquisarPorId(@PathVariable(name = "id") int id){
        try{
            Produto produto = produtoServices.buscarProdutoPeloId(id);
            return produto;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@RequestBody @Valid Produto produto, @PathVariable(name = "id") int id){
        try {
            Produto produtoDB = produtoServices.atualizarProduto(id, produto);
            return produtoDB;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarProduto(@PathVariable(name = "id") int id){
        try{
            produtoServices.deletarProduto(id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
