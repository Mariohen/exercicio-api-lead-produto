package br.com.lead.collector.services;

import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoServices {

    @Autowired
    private ProdutoRepository produtoRepository;

    //Salvar produto
    public Produto salvarProduto(Produto produto){
        Produto objetoProduto = produtoRepository.save(produto);
        return objetoProduto;
    }

    //retorno de todos os registros
    public Iterable<Produto> lerTodosOsProdutos(){
        return produtoRepository.findAll();
    }

    //Retorno buscando por ID
    public Produto buscarProdutoPeloId(int id){
        Optional<Produto> produtoOptional = produtoRepository.findById(id);

        if(produtoOptional.isPresent()){
            Produto produto = produtoOptional.get();
            return produto;
        }else {
            throw new RuntimeException("O Produto não foi encontrado");
        }
    }

    //Atualizar produto
    public Produto atualizarProduto(int id, Produto produto){
        Produto produtoDB = buscarProdutoPeloId(id);

        produto.setId(produtoDB.getId());
        return produtoRepository.save(produto);
    }

    //Deletar produto
    public void deletarProduto(int id){
        if(produtoRepository.existsById(id)) {
            produtoRepository.deleteById(id);
        }else {
            throw new RuntimeException("Registro não existe");
        }
    }

}
