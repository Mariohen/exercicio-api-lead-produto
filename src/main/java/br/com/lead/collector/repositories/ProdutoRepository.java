package br.com.lead.collector.repositories;

import br.com.lead.collector.models.Produto;
import org.springframework.data.repository.CrudRepository;

//É necessario passar o nome da tabela e a chave primaria
public interface ProdutoRepository extends CrudRepository<Produto, Integer> {
}
