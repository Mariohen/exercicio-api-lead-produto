package br.com.lead.collector.repositories;

import br.com.lead.collector.models.Lead;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

//classe que repesenta a tabela e a tipagem da chave primaria(se a chave fosse String, seria String...)
//Interface de comunicacao com o banco de dados
public interface LeadRepository extends CrudRepository <Lead, Integer>{

    //spring usa o nome do metodo para fazer a pesquisa no banco
    //cpf = where de um select, (String cpf) -- argumento de pesquisa

      Lead findFirstByCpf(String cpf);   //--> um unico registro
    // Lead findByCpfAndNome(String cpf, String nome); --> mais de uma condicao

    //    forma para montar uma query
    //    @Query(value = "select * from lead where email = :email")
    //    Lead buscarPorEmail(String email);

    //List<Lead> findByCpf(String cpf); --> lista
    //List<Lead> findByCpf(String cpf);


    List<Lead> findByProdutosId(int id);


}


