package br.com.lead.collector.services;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.lang.reflect.Array;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTeste {

    //Moca algumas classes isolando os testes, neste caso está manipulando LeadRepository e ProdutoRepository
    @MockBean
    private LeadRepository leadRepository;
    @MockBean
    private ProdutoRepository produtoRepository;

    @MockBean
    private CadastroDeLeadDTO cadastroDeLeadDTO;



    //Usa o Autowired para pegar o objeto verdadeiro, neste caso o objeto verdadeiro da LeadService
    @Autowired
    private LeadService leadService;

    // declarando apenas o atributo lead
    Lead lead;
    Produto produto;

    List<Produto> produtos;

    @BeforeEach
    public void setUp(){
        this.lead = new Lead();
        lead.setId(1);
        lead.setNome("Marvin");
        lead.setDataDeCadastro(LocalDate.now());
        lead.setTelefone("11 9999-8888");
        lead.setCpf("415.669.740-11");
        lead.setEmail("marvin@dontpanic.com");

        this.produto = new Produto();
        produto.setId(1);
        produto.setPreco(10.00f);
        produto.setNome("Livro Mochileiro da galaxia");
        produto.setDescricao("Livro do mochileiro da galaxia");
        List<Produto> produtos = Arrays.asList(produto);
        lead.setProdutos(produtos);

    }

    @Test
    public void testarBuscardeLeadPeloID(){
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);  // Mockito.anyInt() --> Moca um numero inteiro
        Assertions.assertEquals(lead, leadService.buscarLeadPeloId(1));

        Lead leadObjeto = leadService.buscarLeadPeloId(Mockito.anyInt());

        Assertions.assertEquals(lead, leadObjeto);
        Assertions.assertEquals(lead.getDataDeCadastro(), leadObjeto.getDataDeCadastro());
    }

    @Test
    public void testarBuscaDeLeadPeloIdQueNaoExiste(){
        Optional<Lead> leadOptional = Optional.empty();
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);  // Mockito.anyInt() --> Moca um numero inteiro

        Assertions.assertThrows(RuntimeException.class, () -> {leadService.buscarLeadPeloId(1233);});
    }

    @Test
    public void testarCadastroDeLead(){
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);  // Mockito.anyInt() --> Moca um numero inteiro
        Assertions.assertEquals(lead, leadService.buscarLeadPeloId(1));

        Lead leadObjeto = leadService.buscarLeadPeloId(Mockito.anyInt());

        Assertions.assertEquals(lead, leadObjeto);
        Assertions.assertEquals(lead.getDataDeCadastro(), leadObjeto.getDataDeCadastro());



    }

    @Test
    public void testarSalvarLead(){
        CadastroDeLeadDTO cadastroDeLeadDTO = new CadastroDeLeadDTO();
        this.cadastroDeLeadDTO = new CadastroDeLeadDTO();
        cadastroDeLeadDTO.setNome("Testes Salve");
        cadastroDeLeadDTO.setCpf("415.669.740-11");
        cadastroDeLeadDTO.setEmail("teste@teste.com");
        cadastroDeLeadDTO.setTelefone("11 88888-4444");

        IdProdutoDTO idProdutoDTO = new IdProdutoDTO();
        idProdutoDTO.setId(1);

        List<IdProdutoDTO> idProdutoDTOS = Arrays.asList(idProdutoDTO);
        cadastroDeLeadDTO.setProdutos(idProdutoDTOS);

        Mockito.when(produtoRepository.findAllById(Mockito.anyIterable())).thenReturn(this.produtos);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).then(objeto -> objeto.getArgument(0)); // then(objeto -> objeto.getArgument --> pegar o Lead que de fato esta sendo processo, se nao deixar assim, vai retornar o Lead que está cadastrado @BeforeAll

        Lead testeDeLead = leadService.salvarLead(cadastroDeLeadDTO);

        Assertions.assertEquals("Testes Salve", testeDeLead.getNome());
    }

}
