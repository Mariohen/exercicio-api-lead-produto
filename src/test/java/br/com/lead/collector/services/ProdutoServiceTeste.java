package br.com.lead.collector.services;

import br.com.lead.collector.DTOs.ResumoDeLeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTeste {

    @MockBean
    private Produto produto;

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private ProdutoServices produtoServices;


    @BeforeEach
    public void setUp(){
        produto.setId(1);
        produto.setNome("Produto 1");
        produto.setDescricao("Descricao do produto 1");
        produto.setPreco(25.00f);

    }

    @Test
    public void testarSalvarProduto (){
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).then(objeto -> objeto.getArgument(0));
    }

    @Test
    public void testarLerTodosOsProdutos(){
        List <ProdutoServices> produtoServices = new ArrayList<>();

        Mockito.when(produtoRepository.findAll()).thenReturn(Mockito.anyIterable());
    }

    @Test
    public void testarbuscarProdutoPeloId(){
        Optional<Produto> produtoOptional = Optional.of(produto);
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptional);
        Assertions.assertEquals(produto, produtoServices.buscarProdutoPeloId(1));

        Produto produtoObjeto = produtoServices.buscarProdutoPeloId(Mockito.anyInt());
        Assertions.assertEquals(produto, produtoObjeto);

    }

    @Test
    public void testarbuscarProdutoPeloIdNaoExistente(){
        Optional<Produto> produtoOptional = Optional.empty();
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(produtoOptional);
        Assertions.assertThrows(RuntimeException.class, () -> {
                                     produtoServices.buscarProdutoPeloId(123123);});
    }

}


//    public Produto salvarProduto(Produto produto){
//        Produto objetoProduto = produtoRepository.save(produto);
//        return objetoProduto;
//    }