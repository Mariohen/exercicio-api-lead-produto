package br.com.lead.collector.controller;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.DTOs.ResumoDeLeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(LeadController.class)
public class LeadControllerTeste {

    @MockBean
    private LeadService leadService;

    @MockBean
    private CadastroDeLeadDTO cadastroDeLeadDTO;

     @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;
    List<Produto> produtos;
    List<Lead> Leads;



    @BeforeEach
    private void setUp(){
        this.lead = new Lead();
        lead.setId(1);
        lead.setNome("Marvin");
        lead.setDataDeCadastro(LocalDate.now());
        lead.setTelefone("11 9999-8888");
        lead.setCpf("415.669.740-11");
        lead.setEmail("marvin@dontpanic.com");

        this.produto = new Produto();
        produto.setId(1);
        produto.setPreco(10.00f);
        produto.setNome("Livro Mochileiro da galaxia");
        produto.setDescricao("Livro do mochileiro da galaxia");
        List<Produto> produtos = Arrays.asList(produto);
        lead.setProdutos(produtos);


    }

    @Test
    public void testarBuscaPorID() throws Exception {
        Mockito.when(leadService.buscarLeadPeloId(1)).thenReturn(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                        .contentType(MediaType.APPLICATION_JSON))
                        .andExpect(MockMvcResultMatchers.status().isOk())
                        .andExpect(MockMvcResultMatchers
                        .jsonPath("$.id", CoreMatchers.equalTo(1)));

    }

    @Test
    public void testarBuscarPorIdQueNaoExiste() throws Exception {
        Mockito.when(leadService.buscarLeadPeloId(1)).thenThrow(RuntimeException.class);
        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }


    @Test
    public void testarBuscarTodosOsLeads() throws Exception {

        List<ResumoDeLeadDTO> resumoDeLeadDTOS = new ArrayList<>();

      Mockito.when(leadService.lerTodosOsLeads()).thenReturn(resumoDeLeadDTOS);

      mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$").isArray());      //só o $ significa que é todo o json, tudo que estiver no corpo
    }

    @Test
    public void testarCadastrarLead() throws Exception {
        Mockito.when(leadService.salvarLead(Mockito.any(CadastroDeLeadDTO.class))).thenReturn(lead);

        ObjectMapper objectMapper = new ObjectMapper();

        String leadJson = objectMapper.writeValueAsString(cadastroDeLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .content(leadJson)).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$", CoreMatchers.containsString("Marvim")));

    }


    @Test
    public void testarValidacaoDeCadastroDeLead() throws Exception {
        cadastroDeLeadDTO.setCpf("3243432432");
        cadastroDeLeadDTO.setEmail("@dsfdsf");

        ObjectMapper objectMapper = new ObjectMapper();
        String leadJson = objectMapper.writeValueAsString(cadastroDeLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .content(leadJson))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
//              .andExpect(MockMvcResultMatchers.jsonPath("$.errors[0].field", CoreMatchers
//                                                            .equalTo("email")));
        Mockito.verify(leadService,Mockito.times(0)).salvarLead(Mockito.any(CadastroDeLeadDTO.class));
    }

}
// perform exige que coloque coloque o thrwos Excepttion
//    ObjectMapper mapper = new ObjectMapper();   transforma em Json