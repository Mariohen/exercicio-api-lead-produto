package br.com.lead.collector.controller;

import br.com.lead.collector.DTOs.ResumoDeLeadDTO;
import br.com.lead.collector.controller.ProdutoController;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.services.ProdutoServices;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(ProdutoController.class)
public class ProdutoControllerTeste {

    @MockBean
    private ProdutoServices produtoServices;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testarCadastrarProduto() throws Exception {
        Produto produto = new Produto();
        produto.setId(1);
        produto.setNome("Produto 1");
        produto.setDescricao("Descricao produto 1");
        produto.setPreco(20.00f);

        Mockito.when(produtoServices.salvarProduto(Mockito.any(Produto.class))).thenReturn(produto);

        ObjectMapper objectMapper = new ObjectMapper();

        String produtoJson = objectMapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.post("/produtos")
                .content(produtoJson)).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$", CoreMatchers.containsString("Produto 1")));

        Mockito.verify(produtoServices, Mockito.times(1)).salvarProduto(Mockito.any(Produto.class));
    }

    @Test
    public void testarCadastrarProdutoComErro() throws Exception {
        Produto produto = new Produto();
        produto.setId(1);
        produto.setNome("");
        produto.setDescricao("Descricao produto 1");
        produto.setPreco(20.00f);

        Mockito.when(produtoServices.salvarProduto(Mockito.any(Produto.class))).thenReturn(produto);

        ObjectMapper objectMapper = new ObjectMapper();

        String produtoJson = objectMapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.post("/produtos")
                .content(produtoJson)).andExpect(MockMvcResultMatchers.status().isBadRequest());


        Mockito.verify(produtoServices, Mockito.times(0)).salvarProduto(Mockito.any(Produto.class));
    }


    @Test
    public void testarPesquisarPorId() throws Exception {
        Produto produto = new Produto();
        produto.setId(1);
        produto.setNome("Produto 1");
        produto.setDescricao("Descricao produto 1");
        produto.setPreco(20.00f);

        Mockito.when(produtoServices.buscarProdutoPeloId(1)).thenReturn(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarPesquisarPorIdQueNaoExiste() throws Exception {
        Mockito.when(produtoServices.buscarProdutoPeloId(1)).thenThrow(RuntimeException.class);
        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    public void testarLerTodosOsProdutos() throws Exception {

        List<Produto> produtos = new ArrayList<>();
        Produto produto = new Produto();
        produto.setId(1);
        produto.setDescricao("Camiseta");
        produto.setNome("Descição da camiseta");
        produto.setPreco(10.0f);
        produtos.add(produto);

        Mockito.when(produtoServices.lerTodosOsProdutos()).thenReturn(produtos);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$").isArray());
    }

    @Test
    public void testarLerTodosOsProdutosListaVazia() throws Exception {

        List<Produto> produtos = new ArrayList<>();

        Mockito.when(produtoServices.lerTodosOsProdutos()).thenReturn(produtos);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers
                        .jsonPath("$").isArray());
    }


    @Test
    public void testarAtualizarProduto() throws Exception {
        Produto produto = new Produto();
       produto.setNome("Produto 1");
        produto.setDescricao("Descricao produto 1");
        produto.setPreco(20.00f);

        Mockito.when(produtoServices.atualizarProduto(Mockito.anyInt(), Mockito.any(Produto.class))).thenReturn(produto);

        ObjectMapper mapper = new ObjectMapper();

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());

        Mockito.verify(produtoServices, Mockito.times(1)).atualizarProduto(Mockito.anyInt(), Mockito.any(Produto.class));

    }


}
